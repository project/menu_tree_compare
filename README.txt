INTRODUCTION
============

The Menu Tree Compare module allows you to visually compare two menu trees by
going to admin/structure/menu/tree_compare.

This may be useful if you use several menu trees for different languages, and
you want to compare the structure / order of the items.


LIBRARIES
=========

The following external libraries have been used:

jQuery 1.11.2
https://github.com/jquery/jquery
https://github.com/jquery/jquery/blob/master/LICENSE.txt
MIT
Whitelisted: https://www.drupal.org/node/1597498

jQuery UI 1.11.2
https://github.com/jquery/jquery-ui
https://github.com/jquery/jquery-ui/blob/master/LICENSE.txt
MIT
Whitelisted: https://www.drupal.org/node/1403540

jsPlumb 1.7.2
https://github.com/sporritt/jsplumb
https://github.com/sporritt/jsPlumb/blob/master/jsPlumb-GPLv2-LICENSE.txt
https://github.com/sporritt/jsPlumb/blob/master/jsPlumb-MIT-LICENSE.txt
GPLv2 & MIT
Whitelisted: https://www.drupal.org/node/2454981

jsTree 3.0.9
https://github.com/vakata/jstree
https://github.com/vakata/jstree/blob/master/LICENSE-MIT
MIT
Whilelisted: https://www.drupal.org/node/1904790


INSTALLATION
============

First download the listed libraries into your libraries folder by running:

"drush make --no-core menu_tree_compare.make"

Then proceed with module installation as usual.


THIRD-PARTY GPL ASSETS
========================

1. ok.png
Rachel Fu / Creative 9 Design. Released under GPL.
https://www.iconfinder.com/icons/13487/cancel_delete_forbidden_icon#size=24
http://kde-look.org/usermanager/search.php?username=c9d

2. not-found.png
Rachel Fu / Creative 9 Design. Released under GPL.
https://www.iconfinder.com/icons/13487/cancel_delete_forbidden_icon#size=24
http://kde-look.org/usermanager/search.php?username=c9d

3. no-parent-match.png
Gnome Project. Released under GPL.
https://www.iconfinder.com/icons/21181/dialog_gtk_warning_icon#size=16

4. no-weight-match.png
Gnome Project. Released under GPL.
https://www.iconfinder.com/icons/20550/desc_descending_gtk_order_sort_icon#size=16
