core = 7.x
api = 2

libraries[jquery][type] = "libraries"
libraries[jquery][download][type] = "file"
libraries[jquery][download][url] = "https://code.jquery.com/jquery-1.11.2.min.js"
libraries[jquery][directory_name] = "jquery-1.11.2"

libraries[jquery_ui][type] = "libraries"
libraries[jquery_ui][download][type] = "file"
libraries[jquery_ui][download][url] = "https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"
libraries[jquery_ui][directory_name] = "jquery_ui-1.11.2"

libraries[jsplumb][type] = "libraries"
libraries[jsplumb][download][type] = "file"
libraries[jsplumb][download][url] = "https://raw.githubusercontent.com/sporritt/jsPlumb/1.7.2/dist/js/jquery.jsPlumb-1.7.2-min.js"
libraries[jsplumb][directory_name] = "jsplumb-1.7.2"

libraries[jstree][type] = "libraries"
libraries[jstree][download][type] = "file"
libraries[jstree][download][url] = "https://github.com/vakata/jstree/zipball/3.0.9"
libraries[jstree][directory_name] = "jstree-3.0.9"
